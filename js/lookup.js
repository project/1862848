/* ===========================================================
* lookup.js
* ===========================================================
* Copyright 2013 Jordan and Huan
*
* ========================================================== */

(function($){
  $.fn.lookup=function(options){
    var opts = $.extend({}, $.fn.lookup.defaults, options);

    return this.each(function(){
      var elmId = $(this).attr('id');

      if($(this).data('plugin')){
        return;
      }

      //create lookup object
      var myplugin = new lookupPlugin(elmId, opts.viewId, opts.selectCallback);

      myplugin.setUp.call(myplugin);

      $(this).data('plugin', myplugin);

    });
  };

  $.fn.lookup.defaults = {
    selectCallback: function(selectedList){
      $(this).val(selectedList[0]);
    }
  };


  /------------------------------------------------------------------------------------------------------------------------/
  var lookupPlugin = function(inputId,viewId,selectCallback){
	/*******************Memeber Properies********************************************/
    this.mInputId = inputId;
    this.mViewId = viewId;
    this.mSelectCallback = selectCallback;

    /********************Pubilc Functions********************************************/
    this.setUp = function(){
      var lookupLink = findBrowseLink.call(this);
      var ownerContext = this;
      $(lookupLink).click(function(){
        var frameId = drawBrowseFrame();
        $('#' + frameId).attr('triggerElement', ownerContext.mInputId);
        showBrowse.call(ownerContext,frameId);
      });
    };

    this.refresh = function(){
      addSelectLink.call(this,'lookup_frame');
    };

    /************************Private Functions***************************************/
    function findBrowseLink(){
      return $('#' + this.mInputId).parent('div').children('span').children('a');
    };

    function drawBrowseFrame(){
      var frame = '<div id="lookup_frame" title="Lookup Browse">';
      frame += '<div id="lookup_content_wrapper">';
      frame += '<div class="browse-loading"></div></div></div>';
      $(frame).appendTo('body');
      return 'lookup_frame';
    };

    function showBrowse(frameId){
      getViewContent.call(this,frameId);

      var frameSeletor = '#' + frameId;
      $(frameSeletor).dialog({
        maxHeight: 700,
        minHeight: 200,
        width: 800,
        modal: true,
        close: function() {
          $(frameSeletor).remove();
        }
      });
    };

    function getViewContent(frameId){
      var ownerContext = this;
      $.ajax({
        type: "GET",
        url: "?q=lookup/get/views",
        data: {
          "viewId": this.mViewId
        },
        dataType: "html",
        success: function(htmlData){
          //replace the content of lookup_content_wrapper with htmldata
          $('#lookup_content_wrapper').html(htmlData);
          addSelectLink.call(ownerContext,frameId);
        }
      });
    };

    function addSelectLink(frameId){
      var ownerContext = this;
      $('#lookup_content_wrapper .view-content table tbody tr td:first-child')
      .each(function(){
        var inputSelector = '#' + ownerContext.mInputId;
        var value = $(this).text();
        $(this).text('');
        var link = '<a href="#">' + value + '</a>';
        $(this).html(link);

        //Add click event to the link of first column
        $(this).children('a').click(function(){
          handleSelect.call(ownerContext, this, inputSelector);
          $('#' + frameId).dialog("close");
        });
      });
    };

    function handleSelect(linkElement,inputSelector){
      var list = new Array();
      $(linkElement).parent().parent().children().each(function(index){
        list[index] = $.trim($(this).text());
      });

      this.mSelectCallback.call($(inputSelector).get(0),list);
    };
    
  };






  /------------------------------------------------------------------------------------------------------------------------/
  /**
	   * Reload js event when it is back from ajax
	   */
  Drupal.behaviors.ajaxRefresh = {
    attach: function (context, settings) {
      var triEle = $('#lookup_frame').attr('triggerElement');
      var myLookup = $('#' + triEle).data('plugin');
      if(myLookup){
        myLookup.refresh.call(myLookup);
      }
    }
  }

})(jQuery);
