Overview:
In enterprise web application, there are usually lots of elements on 
the UI need to be filled in. And sometimes the input value for an element
is a code, and users maybe not familiar with the code, so they need a
popup lookup browse to search what the code they need to be fill into
the UI. And when users select the code, the return data includes many
other columns listed in the lookup browse like name, description and
so on. 
For example, there are some customer related fields on the
UI which need to be filled in. The first one is 'Customer Code', the
second one is 'Customer Name', and 'Customer Address' is the third
one. In this case if you employee lookup module, there would be a
lookup created beside the 'Customer Code' field, and when you click
the lookup image link, there would a lookup browse popup and display
the customer views(predefined via views module), then you can select
one customer listed on the views, and the three customer related
information would be filled back to UI fields automatically by lookup
module.


Features:
�� Create a lookup image link after a textfield type form element.
�� Popup a lookup browse (modal dialog) to display a custom views 
when user clicks the image link.
�� Automatically generate a link for the first column on the lookup browse, 
and this link is for user to pick one row data to fill back the main UI fields
�� Automatically fill the selected data back to the main UI (a callback
function needs to be provided, or else a default fill back logic would be
executed)


Requirements:
Depend on Drupal 7 views module. The display data format on browse 
is table format of views.


Development Guide:
How to use this module for your module development?
It's very easy, just two things you need to do.
First, when you create your form textfield element via form API,
please specify the attribute #lookup to TRUE.
Example:
  $form['city'] = array(
    '#lookup' => TRUE,
    '#type' => 'textfield',
    '#title' => t('City'),
    '#size' => 12,
    '#default_value' => isset($form_state['values']['city']) ?
      $form_state['values']['city'] : NULL,
  );

Second, invoke Lookup.setUp javascript function in your javascript.
This function includes three parameters:
First parameter : the id of the input which you want to add lookup for it.
Second parameter: the name of view you want to display on lookup browse.
Three parameter: the callback funtion used to bind with selectback event,
there are two parameter for this callback
which are event object and selected array with the row data you selected
from the browse.
Example:
    Lookup.setUp('edit-city', 'city_list', function(event,selectedList){
    	$('#edit-city').val(selectedList[0]);
    	$('#edit-country').val(selectedList[2]);
    	$('#edit-continent').val(selectedList[4]);
    });

More detail about how to use you can take a look at
the lookup test module for an example.
