<?php

/**
 * @file
 * Detail implementation for menu callback for views.
 */

/**
 * Call views API to load the views content.
 */
function lookup_browse_load_views() {

  $result = '';
  if (!empty($_REQUEST['viewId'])) {
    $view_id = $_REQUEST['viewId'];
    $result = views_embed_view($view_id);
  }

  return $result;

}
