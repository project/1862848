<?php

function lookup_browse_sample_views_default_views() {

  $view = new view();
  $view->name = 'city_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'city_list';
  $view->human_name = 'City List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'City List';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '2';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: City List: id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'city_list';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: City List: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'city_list';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: City List: City Code */
  $handler->display->display_options['fields']['code']['id'] = 'code';
  $handler->display->display_options['fields']['code']['table'] = 'city_list';
  $handler->display->display_options['fields']['code']['field'] = 'code';
  /* Field: City List: Country */
  $handler->display->display_options['fields']['country']['id'] = 'country';
  $handler->display->display_options['fields']['country']['table'] = 'city_list';
  $handler->display->display_options['fields']['country']['field'] = 'country';
  /* Field: City List: Country Code */
  $handler->display->display_options['fields']['country_code']['id'] = 'country_code';
  $handler->display->display_options['fields']['country_code']['table'] = 'city_list';
  $handler->display->display_options['fields']['country_code']['field'] = 'country_code';
  /* Field: City List: Continent */
  $handler->display->display_options['fields']['continent']['id'] = 'continent';
  $handler->display->display_options['fields']['continent']['table'] = 'city_list';
  $handler->display->display_options['fields']['continent']['field'] = 'continent';
  /* Filter criterion: City List: Country */
  $handler->display->display_options['filters']['country']['id'] = 'country';
  $handler->display->display_options['filters']['country']['table'] = 'city_list';
  $handler->display->display_options['filters']['country']['field'] = 'country';
  $handler->display->display_options['filters']['country']['exposed'] = TRUE;
  $handler->display->display_options['filters']['country']['expose']['operator_id'] = 'country_op';
  $handler->display->display_options['filters']['country']['expose']['label'] = 'Country';
  $handler->display->display_options['filters']['country']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['country']['expose']['operator'] = 'country_op';
  $handler->display->display_options['filters']['country']['expose']['identifier'] = 'country';
  $handler->display->display_options['filters']['country']['expose']['remember_roles'] = array(
      2 => '2',
      1 => 0,
      3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/lookupsample/browse/city_list';


  // Add view to list of views to provide.
  $views[$view->name] = $view;

  // At the end, return array of default views.
  return $views;
}