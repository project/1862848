/**
 * Document ready method
 */

(function($) {
      $('#edit-city').lookup({
    	  viewId:'city_list',
    	  selectCallback:selectCallback1
      });

      $('#edit-continent').lookup({
    	  viewId:'city_list'
      });

      function selectCallback1(selectedList){
          $('#edit-city').val(selectedList[0]);
          $('#edit-country').val(selectedList[2]);
          $('#edit-continent').val(selectedList[4]);
      };

})(jQuery);
